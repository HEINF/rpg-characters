# RPG_Characters
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

# Purpose
This project was made in order to learn the basics of C# and the .Net framework, with an emphasis on Unit testing

## Target Framework
This project has been made targeting .Net Framework 6.

## Install
Clone project into Visual Studio or your choice of compatible editor.

## Usage
The projects purpose is for Unit testing, therefore the actual program only runs a HelloWorld console writeline. 
Unit tests can however be run by selecting RPG_CharactersTests and then right clicking on CharacterTests.cs and choosing 'Run'.

## Contributing
Not open for contributions.

## License
Unknown


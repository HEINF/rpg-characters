using RPG_Characters;
using Xunit;

namespace RPG_CharactersTests
{
    public class CharacterTests
    {
        #region BaseAttributeTests
        [Fact]
        public void NewlyCreatedCharacter_ShouldStartAtLevelOne()
        {
            //Arrange and Act
            Warrior testWarrior = new Warrior("Kratos");
            int expected = 1;
            int actual = testWarrior.Level;

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CharacterLevelingUp_ShouldGainOneLevel()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Kratos");
            int expected = 2;

            //Act
            testWarrior.LevelUp();

            //Assert
            Assert.Equal(expected, testWarrior.Level);
        }

        [Fact]
        public void MageIsCreatedWith_CorrectBaseAttributes()
        {
            //Arrange and Act
            Mage testMage = new Mage("Merlin");
            PrimaryAttributes expectedAttributes = new PrimaryAttributes(1, 1, 8);

            //Assert
            Assert.Equal(expectedAttributes.Strength, testMage.BaseAttributes.Strength);
            Assert.Equal(expectedAttributes.Dexterity, testMage.BaseAttributes.Dexterity);
            Assert.Equal(expectedAttributes.Intelligence, testMage.BaseAttributes.Intelligence);
        }

        [Fact]
        public void RangerIsCreatedWith_CorrectBaseAttributes()
        {
            //Arrange and Act
            Ranger testRanger = new Ranger("Aragon");
            PrimaryAttributes expectedAttributes = new PrimaryAttributes(1, 7, 1);

            //Assert
            Assert.Equal(expectedAttributes.Strength, testRanger.BaseAttributes.Strength);
            Assert.Equal(expectedAttributes.Dexterity, testRanger.BaseAttributes.Dexterity);
            Assert.Equal(expectedAttributes.Intelligence, testRanger.BaseAttributes.Intelligence);
        }

        [Fact]
        public void RogueIsCreatedWith_CorrectBaseAttributes()
        {
            //Arrange and Act
            Rogue testRogue = new Rogue("Blood Raven");
            PrimaryAttributes expectedAttributes = new PrimaryAttributes(2, 6, 1);

            //Assert
            Assert.Equal(expectedAttributes.Strength, testRogue.BaseAttributes.Strength);
            Assert.Equal(expectedAttributes.Dexterity, testRogue.BaseAttributes.Dexterity);
            Assert.Equal(expectedAttributes.Intelligence, testRogue.BaseAttributes.Intelligence);
        }

        [Fact]
        public void WarriorIsCreatedWith_CorrectBaseAttributes()
        {
            //Arrange and Act
            Warrior testWarrior = new Warrior("Kratos");
            PrimaryAttributes expectedAttributes = new PrimaryAttributes(5, 2, 1);

            //Assert
            Assert.Equal(expectedAttributes.Strength,testWarrior.BaseAttributes.Strength);
            Assert.Equal(expectedAttributes.Dexterity, testWarrior.BaseAttributes.Dexterity);
            Assert.Equal(expectedAttributes.Intelligence, testWarrior.BaseAttributes.Intelligence);
        }

        #endregion

        #region LeveUpAttributesTests

        [Fact]
        public void MageAttributesIncreases_WhenLevelingUp()
        {
            //Arrange
            Mage testMage = new Mage("Merlin");
            PrimaryAttributes expectedAttributes = new PrimaryAttributes(2, 2, 13);

            //Act
            testMage.LevelUp();

            //Assert
            Assert.Equal(expectedAttributes.Strength, testMage.BaseAttributes.Strength);
            Assert.Equal(expectedAttributes.Dexterity, testMage.BaseAttributes.Dexterity);
            Assert.Equal(expectedAttributes.Intelligence, testMage.BaseAttributes.Intelligence);
        }

        [Fact]
        public void RangerAttributesIncreases_WhenLevelingUp()
        {
            //Arrange
            Ranger testRanger = new Ranger("Aragon");
            PrimaryAttributes expectedAttributes = new PrimaryAttributes(2, 12, 2);

            //Act
            testRanger.LevelUp();

            //Assert
            Assert.Equal(expectedAttributes.Strength, testRanger.BaseAttributes.Strength);
            Assert.Equal(expectedAttributes.Dexterity, testRanger.BaseAttributes.Dexterity);
            Assert.Equal(expectedAttributes.Intelligence, testRanger.BaseAttributes.Intelligence);
        }

        [Fact]
        public void RogueAttributesIncreases_WhenLevelingUp()
        {
            //Arrange
            Rogue testRogue = new Rogue("Blood Raven");
            PrimaryAttributes expectedAttributes = new PrimaryAttributes(3, 10, 2);

            //Act
            testRogue.LevelUp();

            //Assert
            Assert.Equal(expectedAttributes.Strength, testRogue.BaseAttributes.Strength);
            Assert.Equal(expectedAttributes.Dexterity, testRogue.BaseAttributes.Dexterity);
            Assert.Equal(expectedAttributes.Intelligence, testRogue.BaseAttributes.Intelligence);
        }

        [Fact]
        public void WarriorAttributesIncreases_WhenLevelingUp()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Kratos");
            PrimaryAttributes expectedAttributes = new PrimaryAttributes(8, 4, 2);

            //Act
            testWarrior.LevelUp();

            //Assert
            Assert.Equal(expectedAttributes.Strength, testWarrior.BaseAttributes.Strength);
            Assert.Equal(expectedAttributes.Dexterity, testWarrior.BaseAttributes.Dexterity);
            Assert.Equal(expectedAttributes.Intelligence, testWarrior.BaseAttributes.Intelligence);
        }

        #endregion

        #region EquipItemsTests

        [Fact]
        public void EquipingWeaponWithLevelRequitementHigherThanCharacterLevel_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Kratos");
            Weapon testAxe = new Weapon
                (
                    "Common Axe",
                    2,
                    SlotType.Weapon,
                    WeaponType.Axe,
                    7.0,
                    1.1
                );

            //Act and Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeapon(testAxe));
        }

        [Fact]
        public void EquipingArmorWithLevelRequitementHigherThanCharacterLevel_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Kratos");
            Armor testArmor = new Armor
                (
                    "Common plate body armor",
                    2,
                    SlotType.Body,
                    ArmorType.Plate,
                    new PrimaryAttributes(1,0,0)
                );

            //Act and Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.EquipArmor(testArmor));
        }

        [Fact]
        public void EquipingWrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Kratos");
            Weapon testBow = new Weapon
                (
                    "Common Bow",
                    1,
                    SlotType.Weapon,
                    WeaponType.Bow,
                    12.0,
                    0.8
                );

            //Act and Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeapon(testBow));
        }

        [Fact]
        public void EquipingWrongArmorType_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Kratos");
            Armor testArmor = new Armor
                (
                    "Common cloth head armor",
                    1,
                    SlotType.Head,
                    ArmorType.Cloth,
                    new PrimaryAttributes(0, 0, 5)
                );

            //Act and Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.EquipArmor(testArmor));
        }
        [Fact]
        public void EquippingWeaponSuccessfully_ReturnsSuccessMessage()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Kratos");
            Weapon testAxe = new Weapon
                (
                    "Common Axe",
                    1,
                    SlotType.Weapon,
                    WeaponType.Axe,
                    7.0,
                    1.1
                );
            string expected = "New weapon equipped!";

            //Act and Assert
            Assert.Equal(expected, testWarrior.EquipWeapon(testAxe));

        }

        [Fact]
        public void EquippingArmorSuccessfully_ReturnsSuccessMessage()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Kratos");
            string expected = "New armor equipped!";
            Armor testArmor = new Armor
                (
                    "Common plate body armor",
                    1,
                    SlotType.Body,
                    ArmorType.Plate,
                    new PrimaryAttributes(1, 0, 0)
                );

            //Act and Assert
            Assert.Equal(expected, testWarrior.EquipArmor(testArmor));
        }

        #endregion

        #region DamageTests

        [Fact]
        public void DamageDealt_NoWeaponEquipped()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Kratos");
            double expected = 1.05;

            //Act and Assert
            Assert.Equal(expected, testWarrior.DealDamage());
        }

        [Fact]
        public void DamageDealt_WithValidWeaponEquipped()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Kratos");
            double expected = 8.085;
            Weapon testAxe = new Weapon
                (
                    "Common Axe",
                    1,
                    SlotType.Weapon,
                    WeaponType.Axe,
                    7.0,
                    1.1
                );
            testWarrior.EquipWeapon(testAxe);

            //Act and Assert
            Assert.Equal(expected, testWarrior.DealDamage());
        }

        [Fact]
        public void DamageDealt_WithValidWeaponAndArmorEquipped()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Kratos");
            double expected = 8.162;
            Weapon testAxe = new Weapon
                (
                    "Common Axe",
                    1,
                    SlotType.Weapon,
                    WeaponType.Axe,
                    7.0,
                    1.1
                );
            Armor testArmor = new Armor
                (
                    "Common plate body armor",
                    1,
                    SlotType.Body,
                    ArmorType.Plate,
                    new PrimaryAttributes(1, 0, 0)
                );
            testWarrior.EquipWeapon(testAxe);
            testWarrior.EquipArmor(testArmor);

            //Act and Assert
            Assert.Equal(expected, testWarrior.DealDamage());
        }

        #endregion
    }
}
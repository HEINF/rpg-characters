﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Armor : Item
    {
        public ArmorType Type { get; set; }
        public PrimaryAttributes ArmorAttributes { get; set; }
        /// <summary>
        /// Constructor to instantiate a new Armor object
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="fitsSlot"></param>
        /// <param name="type"></param>
        /// <param name="attributes"></param>
        public Armor(string name, int requiredLevel, SlotType fitsSlot, ArmorType type, PrimaryAttributes attributes)
        {
            this.Name = name;
            this.RequiredLevel = requiredLevel;
            this.FitsSlot = fitsSlot;
            this.Type = type;
            this.ArmorAttributes = attributes;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class PrimaryAttributes
    {
        public double Strength { get; set; }
        public double Dexterity { get; set; }
        public double Intelligence { get; set; }

        public PrimaryAttributes(double strength = 0, double dexterity = 0, double intelligence = 0)
        {
            this.Strength = strength;
            this.Dexterity = dexterity;
            this.Intelligence = intelligence;
        }

        /// <summary>
        /// Adds strenght, dexterity and/or intelligence. Defaults to 0 for values not included.
        /// </summary>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        public void Add(double strength = 0, double dexterity = 0, double intelligence = 0)
        {
            this.Strength = this.Strength + strength;
            this.Dexterity = this.Dexterity + dexterity;
            this.Intelligence = this.Intelligence + intelligence;
        }

        /// <summary>
        /// Adds attributes of a given PrimaryAttribute to this PrimaryAttribute
        /// </summary>
        /// <param name="attributes"></param>
        public void Add(PrimaryAttributes attributes)
        {
            this.Strength = this.Strength + attributes.Strength;
            this.Dexterity = this.Dexterity + attributes.Dexterity;
            this.Intelligence = this.Intelligence  + attributes.Intelligence;
        }
    }
}

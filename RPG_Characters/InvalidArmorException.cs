﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException()
        {

        }
        /// <summary>
        /// If Hero level is insuficient to equip armor, creates message to this effect
        /// </summary>
        /// <param name="level"></param>
        public InvalidArmorException(int level)
            : base("You must be level "+level+" to equip this armor")
        {

        }
        /// <summary>
        /// If armorS is of an incompatible type, create message to this effect
        /// </summary>
        /// <param name="type"></param>
        public InvalidArmorException(ArmorType type)
            : base("You can't equip "+type)
        {

        }
    }
}

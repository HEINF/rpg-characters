﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Warrior : Hero
    {
        protected int BASE_STRENGTH = 5;
        protected int BASE_DEXTERITY = 2;
        protected int BASE_INTELLIGENCE = 1;

        protected int LEVEL_GAIN_STRENGTH = 3;
        protected int LEVEL_GAIN_DEXTERITY = 2;
        protected int LEVEL_GAIN_INTELLIGENCE = 1;

        protected List<WeaponType> compatibleWeapons = new List<WeaponType>()
        {
            WeaponType.Axe,
            WeaponType.Hammer,
            WeaponType.Sword
        };

        protected List<ArmorType> compatibleArmor = new List<ArmorType>()
        {
            ArmorType.Mail,
            ArmorType.Plate
        };
        /// <summary>
        /// Instantiates a Hero of type Warrior
        /// </summary>
        /// <param name="name"></param>
        public Warrior(string name)
        {
            this.Name = name;
            this.BaseAttributes = new PrimaryAttributes(BASE_STRENGTH, BASE_DEXTERITY, BASE_INTELLIGENCE);
            this.Slot = new Slot();
            this.CalculateTotalAttributes();
        }
        /// <summary>
        /// Calculates TotalAttributes. TotalAttributes is the BaseAttributes (inlcuding level Attributes) combined with any bonus Attributes from equipped Armor.
        /// </summary>
        public override void CalculateTotalAttributes()
        {
            this.TotalAttributes = new PrimaryAttributes();
            this.TotalAttributes.Add(this.BaseAttributes);
            foreach (var item in this.Slot.Slots)
            {
                if (item.Key != SlotType.Weapon)
                {
                    var temp = (Armor)item.Value;
                    this.TotalAttributes.Add(temp.ArmorAttributes);
                }
            }
        }
        /// <summary>
        /// Calculates and returns damage done by the Hero, based on Weapon equipped and TotalAttributes
        /// </summary>
        /// <returns>Double Damage</returns>
        public override double DealDamage()
        {
            if (this.Slot.Slots.ContainsKey(SlotType.Weapon))
            {
                var tempWeapon = (Weapon)this.Slot.Slots[SlotType.Weapon];
                return tempWeapon.DPS * (1 + this.TotalAttributes.Strength / 100);
            }
            else
            {
                return 1 * (1 + this.TotalAttributes.Strength / 100);
            }

        }
        /// <summary>
        /// Attempts to Equip the Hero with armor
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>String ArmorEquippedMessage</returns>
        /// <exception cref="InvalidArmorException">If Armor is of an incompatible type or requires a higher level than the Hero is at time of equipment</exception>
        public override string EquipArmor(Armor armor)
        {
            try
            {
                if (!compatibleArmor.Contains(armor.Type))
                {
                    throw new InvalidArmorException(armor.Type);
                }
                if (armor.RequiredLevel > this.Level)
                {
                    throw new InvalidArmorException(armor.RequiredLevel);
                }
                this.Slot.EquipArmor(armor);
                this.CalculateTotalAttributes();
                return "New armor equipped!";

            }
            catch (InvalidArmorException ex)
            {

                Console.WriteLine(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// Attempts to Equip the Hero with a Weapon
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>String WeaponEquippedMessage</returns>
        /// <exception cref="InvalidWeaponException">If Weapon is of an incompatible type or requires a higher level than the Hero is at time of equipment</exception>
        public override string EquipWeapon(Weapon weapon)
        {
            try
            {
                if (!compatibleWeapons.Contains(weapon.Type))
                {
                    throw new InvalidWeaponException(weapon.Type);
                }
                if (weapon.RequiredLevel > this.Level)
                {
                    throw new InvalidWeaponException(weapon.RequiredLevel);
                }
                this.Slot.EquipWeapon(weapon);
                return "New weapon equipped!";

            }
            catch (InvalidWeaponException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// Levels up the Hero by adding level gain attributes to BaseAttributes. Then initiates recalculation of TotalAttributes
        /// </summary>
        public override void LevelUp()
        {
            this.BaseAttributes.Add(LEVEL_GAIN_STRENGTH, LEVEL_GAIN_DEXTERITY, LEVEL_GAIN_INTELLIGENCE);
            this.Level++;
            this.CalculateTotalAttributes();
        }
    }
}


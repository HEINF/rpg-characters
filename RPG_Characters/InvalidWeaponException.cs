﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException()
        {

        }
        /// <summary>
        /// If Hero level is insuficient to equip weapon, creates message to this effect
        /// </summary>
        /// <param name="level"></param>
        public InvalidWeaponException(int level)
            : base("You must be level "+level+" or above to equip this weapon")
        {
        }
        /// <summary>
        /// If Weapon is of an incompatible type, create message to this effect
        /// </summary>
        /// <param name="type"></param>
        public InvalidWeaponException(WeaponType type)
            : base("You can't equip a "+type)
        {
        }
    }
}

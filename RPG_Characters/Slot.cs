﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Slot
    {
        public Dictionary<SlotType, Item> Slots { get; set; }
        
        /// <summary>
        /// Constructor creates new Dictionary<SlotType, Item>
        /// </summary>
        public Slot()
        {
            this.Slots = new Dictionary<SlotType, Item>();
        }
        /// <summary>
        /// Inserts an Item of type Weapon into the Slots Dictionary. SlotType is used as key, with the Weapon Item itself as the value. Replaces Weapon if key exists.
        /// </summary>
        /// <param name="weapon"></param>
        public void EquipWeapon(Weapon weapon)
        {
            this.Slots[weapon.FitsSlot] = weapon;
        }
        /// <summary>
        /// Inserts an Item of type Armor into the Slots Dictionary. SlotType is used as key, with the Armor Item itself as the value. Replaces Armor if key exists.
        /// </summary>
        /// <param name="armor"></param>
        public void EquipArmor(Armor armor)
        {
            this.Slots[armor.FitsSlot] = armor;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Weapon : Item
    {
        public WeaponType Type { get; set; }
        public double BaseDamage { get; set; }
        public double AttackSpeed { get; set; }
        public double DPS { get; set; }
        /// <summary>
        /// Constructor to instantiate new Weapon object
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="fitsSlot"></param>
        /// <param name="type"></param>
        /// <param name="baseDamage"></param>
        /// <param name="attackSpeed"></param>
        public Weapon( string name, int requiredLevel, SlotType fitsSlot, WeaponType type, double baseDamage, double attackSpeed)
        {
            this.Name = name;
            this.RequiredLevel = requiredLevel;
            this.FitsSlot = fitsSlot;
            this.Type = type;
            this.BaseDamage = baseDamage;
            this.AttackSpeed = attackSpeed;
            this.SetDPS();
        }
        /// <summary>
        /// Sets Damage Per Second for the Weapon by multiplying Base Damage with Attack Speed
        /// </summary>
        public void SetDPS()
        {
            this.DPS = this.BaseDamage * this.AttackSpeed;
        }

    }
}

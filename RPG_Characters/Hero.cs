﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; }

        public Slot Slot { get; set; }
        public PrimaryAttributes BaseAttributes { get; set; }
        public PrimaryAttributes TotalAttributes { get; set; }

        /// <summary>
        /// All heroes must start at level 1
        /// </summary>
        protected Hero()
        {
            this.Level = 1;
        }

        public abstract void LevelUp();
        public abstract string EquipWeapon(Weapon item);
        public abstract string EquipArmor(Armor armor);
        public abstract void CalculateTotalAttributes();
        public abstract double DealDamage();
    }
}
